import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {


        // ----------------- write your code BELOW this line only --------
        // your code here (add lines)

        Scanner myScanner = new Scanner(System.in);
        int numberA = myScanner.nextInt();
        int numberB = myScanner.nextInt();
        int numberC = myScanner.nextInt();

        int tmp;

        // Make sure numberA is smaller than numberB
        if (numberA > numberB) {
            tmp = numberA;
            numberA = numberB;
            numberB = tmp;
        }

        // Make sure numberB is smaller than numberC
        if (numberB > numberC) {
            tmp = numberB;
            numberB = numberC;
            numberC = tmp;

            // If replaced, Make sure numberB is still bigger than numberA
            if (numberA > numberB) {
                tmp = numberA;
                numberA = numberB;
                numberB = tmp;
            }

        }

        System.out.println(numberA);
        System.out.println(numberB);
        System.out.println(numberC);
        // ----------------- write your code ABOVE this line only ---------


    }
}
