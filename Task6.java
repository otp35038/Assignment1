// You may not change or erase any of the lines and comments 
// in this file. You may only add lines in the designated 
// area.

import java.util.Scanner;

public class Task6 {


    public static void main(String[] args) {


        // ----------------- "A": write your code BELOW this line only --------
        // your code here (add lines)
        Scanner myScanner = new Scanner(System.in);
        int a = myScanner.nextInt();
        int b = myScanner.nextInt();
        int c = myScanner.nextInt();
        int d = myScanner.nextInt();

        // Make a and b the smallest numbers

        int tmp;

        // Make sure that a is smaller than b
        if (a > b) {
            tmp = a;
            a = b;
            b = tmp;
        }

        // Make sure that b is smaller than c
        if (b > c) {
            tmp = b;
            b = c;
            c = tmp;

            // If replaced, Make sure that a is smaller than b again
            if (a > b) {
                tmp = a;
                a = b;
                b = tmp;
            }

        }

        // Make sure that b is smaller than d
        if (b > d) {
            tmp = b;
            b = d;
            d = tmp;
        }

        System.out.println(a);
        System.out.println(b);
        // ----------------- "B" write your code ABOVE this line only ---------


    } // end of main
} //end of class Task6

