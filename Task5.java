import java.util.Scanner;

public class Task5 {
    public static void main(String args[]) {

        // ----------------- write your code BELOW this line only --------
        // your code here (add lines)
        Scanner myScanner = new Scanner(System.in);
        int a = myScanner.nextInt();
        int b = myScanner.nextInt();
        int c = myScanner.nextInt();

        // Make a and b the smallest numbers
        int tmp;

        // Make sure that b is bigger than a
        if (a > b) {
            tmp = a;
            a = b;
            b = tmp;
        }

        // Make sure that b is smaller than c
        if (b > c) {
            tmp = b;
            b = c;
            c = tmp;
        }

        System.out.println(a);
        System.out.println(b);
        // ----------------- write your code ABOVE this line only ---------

    }
}
