import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {

        // ----------------- write your code BELOW this line only --------
        // your code here (add lines)

        Scanner myScanner = new Scanner(System.in);
        int number = myScanner.nextInt();

        int divisor = 2;

        // Iterating over possible divisors from 2 to the number's square root
        // The divisors will only be primes according to Unique Factorization Theorem
        while (divisor * divisor <= number) {
            // If divisible, print the divisor and divide it from the number
            if ( number % divisor == 0 ) {
                System.out.println(divisor);
                number /= divisor;
            } else {
                // Only increment the divisor when it no longer divides the number
                divisor++;
            }
        }

        // The remaining number is the final divisor
        System.out.println(number);

        // ----------------- write your code ABOVE this line only ---------

    }
}
