// You may not change or erase any of the lines and comments 
// in this file. You may only add lines.

public class Task7 {


    public static void main(String[] args) {


        // ----------------- write any code BELOW this line only --------
        // your code here (add lines)
        boolean isVerified = true;
        int numberA = 0, numberB = 0, numberC = 0, numberD = 0;

        // Creates the 16 possible combinations
        while (numberA <= 1 & isVerified) {
            numberB = 0;

            while (numberB <= 1 & isVerified) {
                numberC = 0;

                while (numberC <= 1 & isVerified) {
                    numberD = 0;

                    while (numberD <= 1 & isVerified) {
                        int a, b, c, d;
                        a = numberA;
                        b = numberB;
                        c = numberC;
                        d = numberD;

                        // ----------------- write any code ABOVE this line only ---------


                        // -----------------  copy here the code from Task 6 that is between
                        // -----------------  the comments "A" and "B"
                        // code from Task 6 here

                        // Make a and b the smallest numbers
                        int tmp;

                        // Make sure that a is smaller than b
                        if (a > b) {
                            tmp = a;
                            a = b;
                            b = tmp;
                        }
                        // Make sure that b is smaller than c
                        if (b > c) {
                            tmp = b;
                            b = c;
                            c = tmp;

                            // If replaced, Make sure that a is smaller than b again
                            if (a > b) {
                                tmp = a;
                                a = b;
                                b = tmp;
                            }
                        }

                        // Make sure that b is smaller than d
                        if (b > d) {
                            tmp = b;
                            b = d;
                            d = tmp;
                        }

                        // -----------------  end of copied code from Task 6


                        // ----------------- write any code BELOW this line only --------
                        // your code here (add lines)

                        if (a > c | a > d | b > c | b > d) {
                            isVerified = false;
                        }

                        numberD++;
                    }

                    numberC++;
                }

                numberB++;
            }

            numberA++;
        }

        if (isVerified) {
            System.out.println("verified");
        } else {
            System.out.println((numberA - 1));
            System.out.println((numberB - 1));
            System.out.println((numberC - 1));
            System.out.println((numberD - 1));
        }

        // ----------------- write any code ABOVE this line only ---------

    } // end of main
} //end of class Task7

