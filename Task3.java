import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {


        // ----------------- write your code BELOW this line only --------
        // your code here (add lines)
        Scanner myScanner = new Scanner(System.in);
        int numerA = myScanner.nextInt();
        int denomA = myScanner.nextInt();
        int numerB = myScanner.nextInt();
        int denomB = myScanner.nextInt();

        // Calculate the numerator and denominator of the result summary
        int numer = numerA * denomB + denomA * numerB;
        int denom = denomA * denomB;

        // Executing the Euclidean algorithm for GCD of the numerator and denominator for fraction reduction
        int numberA = numer;
        int numberB = denom;
        int remainder = numberA % numberB;

        while (remainder != 0) {
            numberA = numberB;
            numberB = remainder;
            remainder = numberA % numberB;
        }

        // numberB is the GCD, Reduce the result fraction
        numer = numer / numberB;
        denom = denom / numberB;

        System.out.println(numer);
        System.out.println(denom);
        // ----------------- write your code ABOVE this line only ---------


    }
}
